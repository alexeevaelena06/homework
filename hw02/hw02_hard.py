import re

# Задание-1: уравнение прямой вида y = kx + b задано в виде строки.
# Определить координату y точки с заданной координатой x.
# вычислите и выведите y

equation = 'y = -12x + 11111140.2121'
x = 2.5

new_eq = equation.replace(' ', '')

f = (re.compile('[+-]?\d*[.]?\d*x').findall(new_eq))[0][:-1]
if not len(f):
    k = 1.0
elif f == '-':
    k = -1.0
else:
    k = float(f)

bar = (re.compile('[^y=]\d*[.]?\d*x?').findall(new_eq))
for value in bar:
    if value.find('x') == -1:
        b = float(value)
y = k * x + b

print('Уравнение прямой:\n%s\nпри x = %s\ny = %s' %
      (equation, x, y))


# Задание-2: Дата задана в виде строки формата 'dd.mm.yyyy'.
# Проверить, корректно ли введена дата.
# Условия корректности:
# 1. День должен приводиться к целому числу в диапазоне от 1 до 30(31)
#  (в зависимости от месяца, февраль не учитываем)
# 2. Месяц должен приводиться к целому числу в диапазоне от 1 до 12
# 3. Год должен приводиться к целому положительному числу в диапазоне от 1 до 9999
# 4. Длина исходной строки для частей должна быть в соответствии с форматом 
#  (т.е. 2 символа для дня, 2 - для месяца, 4 - для года)

# Пример корректной даты
date = '01.11.1985'

# Примеры некорректных дат
date = '01.22.1001'
date = '1.12.1001'
date = '-2.10.3001'


days = {i+1 for i in range(31)}
months = {i+1 for i in range(12)}
years = {i+1 for i in range(9999)}

ERROR_FLAG = 0
ERROR_MSG_1 = 'Ошибка! Формат даты некорректен'
ERROR_MSG_2 = 'Ошибка! В {} \'{}\' неправильный ввод'
ERROR_MSG_3 = 'Ошибка! Такой даты не бывает'
ERROR_MSG_4 = 'Ошибка! В этом месяце нет 31-ого числа'
REQUIRES_MSG = '\nТребуемый формат: dd.mm.yyyy\n' \
               'День(01-31).Месяц(01-12).Год(0001-9999)'
CORRECT_MSG = 'Дата верна:'

d = date.split('.')
if len(d) == 3:
    d = {'day': d[0], 'month': d[1], 'year': d[2]}
    if (not len(d['day']) == 2 or
       not len(d['month']) == 2 or
       not len(d['year']) == 4):
            print('{}{}'.format(ERROR_MSG_1, REQUIRES_MSG))
            ERROR_FLAG = 1
else:
    print('{}{}'.format(ERROR_MSG_1, REQUIRES_MSG))
    ERROR_FLAG = 1

if isinstance(d, dict):
    for key, value in d.items():
        if not value.isdigit():
            ERROR_FLAG = 1
            print(ERROR_MSG_2.format(key, value), REQUIRES_MSG)
    if not ERROR_FLAG:
        day = int(d['day'])
        month = int(d['month'])
        year = int(d['year'])

        if not (day in days and
                month in months and
                year in years):
            print(ERROR_MSG_3, REQUIRES_MSG)
        elif (day == 31 and
              month in (2, 4, 6, 9, 11)):
            print(ERROR_MSG_4)
        else:
            print(CORRECT_MSG)

print(date)
days.clear()
months.clear()
years.clear()


# Задание-3: "Перевёрнутая башня" (Задача олимпиадного уровня)
#
# Вавилонцы решили построить удивительную башню —
# расширяющуюся к верху и содержащую бесконечное число этажей и комнат.
# Она устроена следующим образом — на первом этаже одна комната,
# затем идет два этажа, на каждом из которых по две комнаты, 
# затем идёт три этажа, на каждом из которых по три комнаты и так далее:
#         ...
#     12  13  14
#     9   10  11
#     6   7   8
#       4   5
#       2   3
#         1
#
# Эту башню решили оборудовать лифтом --- и вот задача:
# нужно научиться по номеру комнаты определять,
# на каком этаже она находится и какая она по счету слева на этом этаже.
#
# Входные данные: В первой строчке задан номер комнаты N, 1 ≤ N ≤ 2 000 000 000.
#
# Выходные данные:  Два целых числа — номер этажа и порядковый номер слева на этаже.
#
# Пример:
# Вход: 13
# Выход: 6 2
#
# Вход: 11
# Выход: 5 3


N = 11

base_house = [['room']*x for x in range(99) for y in range(x)]

house = [list(enumerate(base_house, start=(i+1))) for
         i in range(1)][0]

room = 1
for floor in house:
    for i in range(len(floor[1])):
        floor[1].remove('room')
        floor[1].append(room)
        room += 1

for floor in house:
    for rooms in floor[1]:
        if rooms == N:
            print('Квартира № %s\nЭтаж %s, %s-я слева' %
                  (N, floor[0], floor[1].index(N) + 1))
